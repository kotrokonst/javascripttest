module.exports = (sequelize, DataTypes) => {
  return sequelize.define('description', {
    id: {
      type: DataTypes.UUID,
      autoIncrement: true,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      defaultValue: 'Default Description',
      allowNull: true
    }
  })
}
